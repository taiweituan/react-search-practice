import {
  SearchResultRaw,
  SearchResultData,
  SearchResultIncluded,
} from '../types/Search'

function searchResultNormalizer(res: SearchResultRaw) {
  const normalizedSearchResult = res.data.reduce(
    (accu: any, d: SearchResultData) => {
      // Error handling
      if (!d?.attributes?.name || !d.relationships.primary_image?.data?.id) {
        return accu
      }

      const name = d?.attributes?.name
      const id = d.relationships.primary_image?.data?.id

      const imageUrl =
        res.included.find((a: SearchResultIncluded) => {
          return a.id === id
        })?.attributes.url || ''

      return [
        ...accu,
        {
          rentalName: name,
          imageUrl,
        },
      ]
    },
    [],
  )

  return normalizedSearchResult
}

export { searchResultNormalizer }
