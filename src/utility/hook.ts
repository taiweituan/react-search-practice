import debounce, { DebouncedFunc } from 'lodash-es/debounce'
import { useEffect, useMemo } from 'react'

function useDebounce<T extends (...args: any[]) => any>(
  apiFn: T,
  searchTerm = '',
): DebouncedFunc<T> {
  const debouncedEventHandler = useMemo(
    () =>
      // const debouncedSearchTerm = useDebounce(searchTerm)
      debounce<T>(apiFn, 1000),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  )

  useEffect(() => {
    return () => {
      debouncedEventHandler.cancel()
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchTerm])

  return debouncedEventHandler
}

export { useDebounce }
