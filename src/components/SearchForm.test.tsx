import React from 'react'
import { act, render, screen, waitFor } from '@testing-library/react'
import '@testing-library/jest-dom'
import '@testing-library/user-event'
import SearchForm from './SearchForm'
import { setupServer } from 'msw/node'
import { rest } from 'msw'
import userEvent from '@testing-library/user-event'

import mockResponseSingle from '../fixture/searchResponseSingle.json'

describe('Search Form', () => {
  const requestUrl = 'https://search.outdoorsy.com/rentals'
  const requestUrlLong = `${requestUrl}?filter[keywords]=trailer&page[limit]=8&page[offset]=0`

  test('loads and displays greeting', async () => {
    render(<SearchForm />)
    expect(screen.getByTestId('landing')).toHaveTextContent(
      'Find you next RV 🚐',
    )
  })

  describe('single response', () => {
    const server = setupServer(
      rest.get(requestUrl, (req, res, ctx) => {
        return res(ctx.status(200), ctx.json(mockResponseSingle))
      }),
    )

    // establish API mocking before all tests
    beforeAll(() => server.listen())
    // reset any request handlers that are declared as a part of our tests
    // (i.e. for testing one-time error scenarios)
    afterEach(() => server.resetHandlers())
    // clean up once the tests are done
    afterAll(() => server.close())

    test('search for RVs by typing in the search bar', async () => {
      const spy = jest.spyOn(global, 'fetch')

      render(<SearchForm />)
      userEvent.type(screen.getByTestId('search-input'), 'trailer')

      act(() => {
        jest.runAllTimers()
      })

      await waitFor(() => expect(spy).toHaveBeenCalledTimes(1))
      await waitFor(() => expect(spy).toHaveBeenCalledWith(requestUrlLong))
      await waitFor(() => {
        expect(screen.getByTestId('loading-icon')).toBeInTheDocument()
      })
      await waitFor(() => {
        expect(screen.getByTestId('search-list')).toBeInTheDocument()
      })
      await waitFor(() => {
        expect(screen.getByText('test1')).toBeInTheDocument()
      })
    })
  })
})
