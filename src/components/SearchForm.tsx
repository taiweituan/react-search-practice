import { useState, ChangeEvent, useEffect } from 'react'
import SearchList from './SearchList'
import { getSearch } from '../api/search'
import { searchResultNormalizer } from '../utility/common'
import { SearchResultNormalized } from '../types/Search'
import LoadingScreen from './loading/loadingScreen'
import { useDebounce } from '../utility/hook'
const SEARCH_LIMIT = 8

function Landing() {
  return (
    <div id="landing" data-testid="landing">
      <h1>Find you next RV &#128656;</h1>
    </div>
  )
}

// when clear out input, result should be cleared

function SearchForm() {
  const [searchTerm, setSearchTerm] = useState('')
  const [searchResult, setSearchResult] = useState<SearchResultNormalized[]>([])
  const [isSearching, setIsSearching] = useState(false)
  const [totalPossibleResult, setTotalPossibleResult] = useState(0)
  const [currentIndex, setCurrentIndex] = useState(0)
  const [hasSearched, setHasSearched] = useState(false)

  const isEndReached = currentIndex >= totalPossibleResult

  const resetSearch = () => {
    setSearchResult([])
    setTotalPossibleResult(0)
    setCurrentIndex(0)
    setHasSearched(false)
  }

  const fetchNewSearchResult = async (term: string) => {
    setIsSearching(true)
    const result = await getSearch(term)
    const normalizedResult = searchResultNormalizer(result)

    setTotalPossibleResult(result.meta.total)
    setCurrentIndex(SEARCH_LIMIT)
    setSearchResult(normalizedResult)
    setIsSearching(false)
    setHasSearched(true)
  }

  const fetchAdditionalResult = async (term: string) => {
    setIsSearching(true)
    const result = await getSearch(term, SEARCH_LIMIT, currentIndex)
    const normalizedResult = searchResultNormalizer(result)

    setCurrentIndex(currentIndex + SEARCH_LIMIT)
    setSearchResult([...searchResult, ...normalizedResult])
    setIsSearching(false)
  }

  const debouncedEventHandler = useDebounce<typeof fetchNewSearchResult>(
    fetchNewSearchResult,
    searchTerm,
  )

  useEffect(() => {
    if (searchTerm !== '') {
      debouncedEventHandler(searchTerm)
    } else {
      resetSearch()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchTerm])

  const handleOnChange = (e: ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(e.target.value)
  }

  const handleLoadMore = () => {
    fetchAdditionalResult(searchTerm)
  }

  return (
    <div className="mb-3">
      <div className="input-group mb-4">
        <input
          autoFocus
          type="text"
          className="form-control"
          id="search-input"
          data-testid="search-input"
          placeholder="Search..."
          value={searchTerm}
          onChange={handleOnChange}
          style={{
            height: '60px',
          }}
        />
      </div>

      {!hasSearched ? <Landing /> : <SearchList results={searchResult} />}
      {isSearching && <LoadingScreen />}
      {!isEndReached && hasSearched && (
        <button
          onClick={handleLoadMore}
          className="btn btn-primary btn-lg col-sm-12"
        >
          Load More
        </button>
      )}
    </div>
  )
}

export default SearchForm
