import { SearchResultNormalized } from '../types/Search'

interface Props {
  results: SearchResultNormalized[]
}

function SearchList(props: Props) {
  return (
    <div id="search-list" data-testid="search-list">
      {props.results.map(({ rentalName, imageUrl }, index: number) => {
        return (
          <div
            className="card mb-4 border-0 justify-content-center"
            key={index}
          >
            <div
              className="row g-0 overflow-hidden"
              style={{
                height: '160px',
              }}
            >
              <div
                className="col-sm-2 col-md-3 bg-dark d-flex flex-column justify-content-center overflow-hidden"
                style={{
                  height: '100%',
                  borderRadius: '.8rem',
                }}
              >
                <img
                  className="img-fluid rounded-start rv-image"
                  src={imageUrl}
                  alt={rentalName}
                  style={{
                    maxWidth: '100%',
                  }}
                />
              </div>
              <div className="col-sm-10 col-md-9 flex-column align-self-center">
                <div className="card-body p-4">
                  <p className="card-text text-start fs-5">{rentalName}</p>
                </div>
              </div>
            </div>
          </div>
        )
      })}
    </div>
  )
}

export default SearchList
