export interface SearchResultData {
  id: string
  type: string
  attributes: {
    name: string
  }
  relationships: {
    primary_image: {
      data: {
        id: string
      }
    }
  }
}

export interface SearchResultIncluded {
  id: string
  attributes: {
    url: string
  }
}

export interface SearchResultRaw {
  data: SearchResultData[]
  included: SearchResultIncluded[]
}

export interface SearchResultNormalized {
  rentalName: string
  imageUrl: string
}
