const SEARCH_URL = `https://search.outdoorsy.com/rentals`

function getSearch(keywords = '', limit = 8, offset = 0) {
  const url = `${SEARCH_URL}?filter[keywords]=${keywords}&page[limit]=${limit}&page[offset]=${offset}`

  return fetch(url)
    .then((response) => response.json())
    .catch((e) => {
      console.error(e.message)
    })
}

export { getSearch }
