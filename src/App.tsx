import './App.css'
import SearchForm from './components/SearchForm'

function App() {
  return (
    <div className="App">
      <div className="container mt-3">
        <SearchForm />
      </div>
    </div>
  )
}

export default App
