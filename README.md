# Todo

- [x] Load more search result or infinity scroll
- [x] Enlarge Image on hover
- [x] add loading animation
- [ ] Sorting result
- [ ] Search result caching
- [ ] Tests Cypress and/or unit tests
- [ ] Maybe refactor to NextJS

## Getting Start

1. Run `npm i` or `yarn`
2. Start the app `npm start` or `yarn start`
